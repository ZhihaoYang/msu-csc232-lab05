/**
 * @file IFib.h
 * @author James R. Daehn
 * @brief An interface for computing values related to the Fibonacci number sequence.
 */

#ifndef MSU_CSC232_LAB05_IFIB_H
#define MSU_CSC232_LAB05_IFIB_H

#include <vector>

class IFib {
public:
    virtual unsigned long getFibonacciNumber(const unsigned long &n) const = 0;
    virtual std::vector<unsigned long> getFibonacciSequence(const unsigned long &n) const = 0;
    virtual ~IFib() {}
};

#endif //MSU_CSC232_LAB05_IFIB_H
