/**
 * @file FibRecursiveImpl.h
 * @author James R. Daehn
 * @brief An implementation of IFib using recursion.
 */

#ifndef MSU_CSC232_LAB05_FIBRECURSIVEIMPL_H
#define MSU_CSC232_LAB05_FIBRECURSIVEIMPL_H

#include "IFib.h"

class FibRecursiveImpl : public IFib {
public:
    virtual unsigned long getFibonacciNumber(const unsigned long &n) const override;
    virtual std::vector<unsigned long> getFibonacciSequence(const unsigned long &n) const override;
    virtual ~FibRecursiveImpl() {}
};

#endif //MSU_CSC232_LAB05_FIBRECURSIVEIMPL_H
