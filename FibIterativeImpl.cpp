//
// Created by James Daehn on 9/26/16.
//

#include "FibIterativeImpl.h"

unsigned long FibIterativeImpl::getFibonacciNumber(const unsigned long &n) const {
    if (n <= 2) {
        return 1;
    }

    unsigned long grandparent{1};
    unsigned long parent{1};
    unsigned long me;

    for (unsigned long i = 2; i < n; ++i) {
        me = parent + grandparent;
        grandparent = parent;
        parent = me;
    }
    return me;
}

std::vector<unsigned long> FibIterativeImpl::getFibonacciSequence(const unsigned long &n) const {
    return std::vector<unsigned long>();
}