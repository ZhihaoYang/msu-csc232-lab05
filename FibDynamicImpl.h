//
// Created by James Daehn on 9/26/16.
//

#ifndef MSU_CSC232_LAB05_FIBDYNAMICIMPL_H
#define MSU_CSC232_LAB05_FIBDYNAMICIMPL_H

#include "IFib.h"

class FibDynamicImpl : public IFib {
public:
    virtual unsigned long getFibonacciNumber(const unsigned long &n) const override;
    virtual std::vector<unsigned long> getFibonacciSequence(const unsigned long &n) const override;
    virtual ~FibDynamicImpl() {}
};


#endif //MSU_CSC232_LAB05_FIBDYNAMICIMPL_H
