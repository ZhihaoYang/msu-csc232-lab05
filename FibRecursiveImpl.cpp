/**
 * @file FibRecursiveImpl.cpp
 * @author James R. Daehn
 * @brief Implementation of FibRecursiveImpl.
 */

#include "FibRecursiveImpl.h"

unsigned long FibRecursiveImpl::getFibonacciNumber(const unsigned long &n) const {
    if (n <= 2) {
        return 1;
    } else {
        return getFibonacciNumber(n - 1) + getFibonacciNumber(n - 2);
    }
}

std::vector<unsigned long> FibRecursiveImpl::getFibonacciSequence(const unsigned long &n) const {
    return std::vector<unsigned long>();
}
